let js = require('fs').readFileSync(require('path').join('./', 'node_modules', 'mimi-downloader', 'app', 'js', 'panel.js'), 'utf8');
let js2 = require('fs').readFileSync(require('path').join('./', 'node_modules', 'mimi-downloader', 'app', 'js', 'danmaku.js'), 'utf8');
let html = require('fs').readFileSync(require('path').join('./', 'node_modules', 'mimi-downloader', 'app', 'panel.html'), 'utf8');

let fns = js2.match(
    /function (.*)\(/gm
);

fns = fns.map(
    f => f.split('').slice(9, -1).join('')
).filter(f => ![
    'addZero'
].includes(f));


js = js.split("\n").slice(0, -2).join("\n");
js = js.replace(/\$\(/g, 'this.$(');
js = js.replace(/fetch\(/g, 'return fetch(');
js2 = js2.replace(/fetch\(/g, 'return fetch(');
js = js.replace('$("#videoName").val(sanitize(this.name));', '$("#videoName").val(sanitize(this.name)); return data;')
js = js.replace(/getDanmaku\(/g, 'this.danmaku.getDanmaku(');
js = js.replace(/\:hidden/g, 'etomon');
js = js.replace(/data\.videoData/g, 'data.video');
js = js.replace(/console\.log\("PLAY URL", data\);/g, 'return target;');
js = js.replace(/showError/gm, 'showError');
js = js.replace('data.video.pages[this.pid - 1]', 'data.video.viewInfo')
js = js.replace('fallback ? this.parseDataFallback', 'return fallback ? this.parseDataFallback');
let $;
let ctx = {
        showError: err => { throw err; },
        fetch: (
            (url, args = {}) => {
                args.headers = args.headers || {};
                args.headers['User-Agent'] = `Mozilla/5.0 (Linux; Android 8.0.0; Pixel 2 XL Build/OPD1.170816.004) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Mobile Safari/537.36`;

                return require('node-fetch')(url, args);
            }
        ),
        console: {
            log: function (){},
            error: function(){}
        }
};

let danmaku = eval(`
  (
        (function ({
            ${Object.keys(ctx).join(',')}
        }, downloader) {
            ${js2}
            
            return {
                ${fns.join(',')}
            }
        })
  )
`).bind(ctx, ctx);

js = `
    (function({
            ${Object.keys(ctx).join(',')}
       }) {
        ${js}
        
        return Downloader;
    })
`;


let DownloaderBase = eval(js).call(ctx, ctx);

class Downloader extends DownloaderBase {
        constructor(videoUrl) {
                super();
                this.$ = require('cheerio').load(html);
                this.$('#videoUrl').val(videoUrl);
                this.danmaku = (danmaku)(ctx, this);
        }

        get videoUrl() { return this.$('#videoUrl').val(); }
        set videoUrl(val) { this.$('#videoUrl').val(val); }


        async getMediaObjects() {
            await this.getAid();
            return this.getData();
        }

        async getMediaUrls() {
            return [].concat(await this.getMediaObjects() || []).map(p => p.url);
        }

        async getMediaUrl() {
            return (await this.getMediaUrls())[0] || null;
        }


        parseData(target) {
            return target.map(part => part.url);
        }
}

module.exports = { Downloader };